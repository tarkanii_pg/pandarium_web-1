package com.playtagon.pandarium_web;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity("emails")
public class Email {
	@Id
	private ObjectId id;
	private String data;

	public Email(String data) {
		super();
		this.data = data;
	}

	public Email() {
		super();
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public void setData(String email) {
		this.data = email;
	}

	public ObjectId getId() {
		return id;
	}

	public String getData() {
		return data;
	}

}
