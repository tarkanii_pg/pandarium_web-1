package com.playtagon.pandarium_web;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.validator.AbstractValidator;

public class EmailValidator extends AbstractValidator {

	@Override
	public void validate(ValidationContext ctx) {
		String emailToValidate = (String) ctx.getProperty().getValue();
		try {
			InternetAddress emailAddr = new InternetAddress(emailToValidate);
			emailAddr.validate();
		} catch (AddressException ex) {
			addInvalidMessage(ctx, "Email has wrong format!");
		}

	}

}
